package br.com.rafael.chucknorrisio.presentation;

import android.os.Handler;

import java.util.ArrayList;
import java.util.List;

import br.com.rafael.chucknorrisio.Colors;
import br.com.rafael.chucknorrisio.MainActivity;
import br.com.rafael.chucknorrisio.datasource.CategoryRemoteDataSource;
import br.com.rafael.chucknorrisio.model.CategoryItem;

public class CategoryPresenter implements CategoryRemoteDataSource.ListCategoriesCallback {


    private final MainActivity view;
    private final CategoryRemoteDataSource dataSource;

    private static List<CategoryItem> items = new ArrayList<>();



    public CategoryPresenter(MainActivity mainActivity, CategoryRemoteDataSource dataSource) {
        this.view = mainActivity;
        this.dataSource = dataSource;
    }

    public void requestAll() {
        //Chamar um servidor http;
        this.view.showProgressBar();
        //this.request();
        this.dataSource.findAll(this);
    }

    @Override
    public void onSuccess(List<String> response) {
        List<CategoryItem> categoryItems = new ArrayList<>();
        for(String val: response) {
            categoryItems.add(new CategoryItem(val, Colors.randomColor()));
        }
        view.showCategories(categoryItems);
    }

    @Override
    public void onError(String message) {
        this.view.showFailure(message);
    }

    @Override
    public void onComplete() {
        view.hideProgressBar();
    }
}
